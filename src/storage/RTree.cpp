/** Aesalon, a tool to visualize program behaviour in real time.
	Copyright (C) 2009-2011, Aesalon development team.
	
	Aesalon is distributed under the terms of the GNU GPLv3. See
	the included file LICENSE for more information.
	
	@file src/storage/RTree.cpp
*/

#include "storage/RTree.h"

namespace Storage {

//RTreeTemplate
//__gnu_cxx::__pool_alloc< RTreeScope::Node > RTreeScope::m_allocator;

} // namespace Storage